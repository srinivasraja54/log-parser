logpath=$1
TO_ADDRESS="srinivasraja.pasupuleti@cognizant.com"
FROM_ADDRESS="logparser@cognizant.com"
SUBJECT="http error count for $logpath exceeded 100"

cat <<'EOF' > time_parser.awk
{
    split($4,t,/[[ :\/]/)
    mthNr = sprintf("%02d",(index("JanFebMarAprMayJunJulAugSepOctNovDec",t[3])+2)/3)
    curTime = t[4] mthNr t[2] t[5] t[6] t[7]
}
curTime >= minTime
EOF

if [ -f "$logpath" ]
then
 echo "Input logfile: $logpath , exists"
#grep log entries for last one hour only. since this script is expected to run every one hour successfully
 awk -v minTime=$(date -d '60 min ago' '+%Y%m%d%H%M%S') -f time_parser.awk $logpath > tempfile
 err_count=$(cat tempfile | awk -e '$9 ~ /[5|4][0-9][0-9]/ {print $9}' | sort | uniq -c | awk '{SUM+=$1}END{print SUM}')
 echo "Number of 4** or 5** events(for last one hour) in given $logpath: $err_count"
 if [[ $err_count > 100 ]]; then
  BODY="http errors(4XX or 5XX) count for the log file $logpath is  calculated as $err_count"
  echo "sending mail alert to $TO_ADDRESS"
  #echo ${BODY} | mail -s ${SUBJECT} ${TO_ADDRESS} -- -r${FROM_ADDRESS}
 else
  echo "$$err_count is within the threshold since last one hour"
 fi
else
 echo "passed logfile is not valid"
fi
